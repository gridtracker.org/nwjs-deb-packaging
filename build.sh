#!/bin/sh

ver="0.60.0"
armextver="nw60"
armintver="0.60.0"
arm32date="2022-01-07"
arm64date="2022-02-18"

# set build arch for pulling right source bins
DEB_ARCH=$(dpkg-architecture -q DEB_BUILD_ARCH)

if [[ ${DEB_ARCH} == "amd64" ]]
then
    EXT_ARCH=x64
    echo "Building nwjs-${ver}-${DEB_ARCH}"
    echo -e "\e[0Ksection_start:`date +%s`:wget[collapsed=true]\r\e[0KGetting Upstream Binaries"
    wget https://dl.nwjs.io/v${ver}/nwjs-v${ver}-linux-x64.tar.gz
    echo -e "\e[0Ksection_end:`date +%s`:wget\r\e[0K"
elif [[ ${DEB_ARCH} == "i386" ]]
then
    EXT_ARCH=ia32
    echo "Building nwjs-${ver}-${DEB_ARCH}"
    echo -e "\e[0Ksection_start:`date +%s`:wget[collapsed=true]\r\e[0KGetting Upstream Binaries"
    wget https://dl.nwjs.io/v${ver}/nwjs-v${ver}-linux-ia32.tar.gz
    echo -e "\e[0Ksection_end:`date +%s`:wget\r\e[0K"
elif [[ ${DEB_ARCH} == "armhf" ]]
then
    EXT_ARCH=arm
    echo "Building nwjs-${armintver}-${DEB_ARCH}"
    echo -e "\e[0Ksection_start:`date +%s`:wget[collapsed=true]\r\e[0KGetting Upstream Binaries"
    wget https://github.com/LeonardLaszlo/nw.js-armv7-binaries/releases/download//${armextver}_${arm32date}/${armextver}_${arm32date}.tar.gz
    tar -xf ${armextver}_${arm32date}.tar.gz
    mv ./usr/docker/dist/nwjs-chromium-ffmpeg-branding/nwjs-v${armintver}-linux-arm.tar.gz ./
    rm -rf ./usr; rm ${armextver}_${arm32date}.tar.gz
    echo -e "\e[0Ksection_end:`date +%s`:wget\r\e[0K"
elif [[ ${DEB_ARCH} == "arm64" ]]
then
	EXT_ARCH=arm64
    echo "Building nwjs-${armintver}-${DEB_ARCH}"
    echo -e "\e[0Ksection_start:`date +%s`:wget[collapsed=true]\r\e[0KGetting Upstream Binaries"
    wget https://github.com/LeonardLaszlo/nw.js-armv7-binaries/releases/download/${armextver}-arm64_${arm64date}/${armextver}-arm64_${arm64date}.tar.gz
    tar -xf ${armextver}-arm64_${arm64date}.tar.gz
    mv ./usr/docker/dist/nwjs-chromium-ffmpeg-branding/nwjs-v${armintver}-linux-arm64.tar.gz ./
    rm -rf ./usr; rm ${armextver}-arm64_${arm64date}.tar.gz
    echo -e "\e[0Ksection_end:`date +%s`:wget\r\e[0K"
else
    exit 73
fi

dpkg-buildpackage -b
cd /
cp nwjs*.buildinfo /build/artifacts/
cp nwjs*.changes /build/artifacts/
cp nwjs*.deb /build/artifacts/

echo "Completed build.sh"

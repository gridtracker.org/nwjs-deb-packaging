FROM debian:stable AS builder
ADD setup.sh /opt/
RUN /bin/bash /opt/setup.sh

VOLUME /build
WORKDIR /build

CMD ["/bin/bash", "/build/build.sh"]
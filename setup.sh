#!/bin/bash

echo "Starting setup.sh"
# install deps
echo -e "\e[0Ksection_start:`date +%s`:apt_get[collapsed=true]\r\e[0KGetting Build Dependencies"
apt-get update && \
apt-get upgrade -y && \
apt-get install -y build-essential git devscripts wget apt-utils

apt-get install -y \
libx11-xcb-dev libxrandr-dev libxcomposite-dev libxcursor-dev libxdamage-dev \
libxi-dev libxss-dev libxtst-dev libasound2-dev libatk1.0-dev libatspi2.0-dev \
libcairo2-dev libcups2-dev libdbus-1-dev libdrm-dev libgbm-dev \
libgdk-pixbuf2.0-dev libgtk-3-dev libpango1.0-dev libxcb-dri3-dev
echo -e "\e[0Ksection_end:`date +%s`:apt_get\r\e[0K"

# show me what we are early on
echo "Host Arch:   $(dpkg-architecture -q DEB_HOST_ARCH)"
echo "Build Arch:  $(dpkg-architecture -q DEB_BUILD_ARCH)"
echo "Target Arch: $(dpkg-architecture -q DEB_TARGET_ARCH)"

echo "Completed setup.sh"